/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/18 21:24:11 by garm              #+#    #+#             */
/*   Updated: 2014/11/20 04:43:28 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int			ft_splitlen(char **split)
{
	int		i;

	if (!split)
		return (ERROR);
	i = 0;
	while (split[i])
		i++;
	return (i);
}

void		ft_splitdel(char ***split)
{
	int		i;

	if (!split || !*split)
		return ;
	i = 0;
	while ((*split)[i])
	{
		ft_memdel((void **)&((*split)[i]));
		i++;
	}
	ft_memdel((void **)split);
}
