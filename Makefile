# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/08/26 23:49:31 by garm              #+#    #+#              #
#    Updated: 2014/11/29 00:36:58 by garm             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

NAME = a.out
LIBS = -lft -lftsock
LIBS_DIR = libft libftsock

SOURCES_DIR = srcs
INCLUDES_DIR = includes

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra
	CC = cc
else
	FLAGS = -Wall -Werror -Wextra -ansi -std=c89
endif

CFLAGS = $(foreach DIR, $(LIBS_DIR), -I ./$(DIR)/$(INCLUDES_DIR) )
CFLAGS += $(FLAGS) -I $(INCLUDES_DIR)

LDFLAGS = $(foreach DIR, $(LIBS_DIR), -L ./$(DIR) )
LDFLAGS += $(LIBS)


DEPENDENCIES = \
			   $(INCLUDES_DIR)/server.h

SOURCES = \
		  $(SOURCES_DIR)/server.c \
		  $(SOURCES_DIR)/utils.c \

OBJS = $(SOURCES:.c=.o)

all: $(NAME)

%.o: %.c $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS) lib
	@echo Creating $(NAME)...
	@$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

lib:
	@$(foreach dir, $(LIBS_DIR), make -C $(dir);)

test:
	@$(foreach DIR, $(LIBS_DIR), echo $(DIR);)

clean:
	@$(foreach dir, $(LIBS_DIR), make clean -C $(dir);)
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

fclean: clean
	@$(foreach dir, $(LIBS_DIR), make cleanbin -C $(dir);)
	@rm -f $(NAME)
	@echo Deleting $(NAME)...

re: fclean all

.PHONY: clean fclean re all test

