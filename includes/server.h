/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/20 04:41:27 by garm              #+#    #+#             */
/*   Updated: 2014/11/20 04:42:57 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVER_H
# define SERVER_H

# include "libft.h"
# include "libftsock.h"

/*
** utils.c
*/
int			ft_splitlen(char **split);
void		ft_splitdel(char ***split);

#endif
